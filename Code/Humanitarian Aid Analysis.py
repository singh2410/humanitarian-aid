#!/usr/bin/env python
# coding: utf-8

# # Project- Humanitarian Aid - Clustering & PCA
# ## By- Aarush Kumar
# ### Dated: April 14,2022

# In[1]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
# Import the StandardScaler()
from sklearn.preprocessing import StandardScaler
#Improting the PCA module
from sklearn.decomposition import PCA
from sklearn.decomposition import IncrementalPCA
from sklearn.neighbors import NearestNeighbors
from random import sample
from numpy.random import uniform
from math import isnan
# To perform KMeans clustering 
from sklearn.cluster import KMeans
from scipy.cluster.hierarchy import linkage
from scipy.cluster.hierarchy import dendrogram
from scipy.cluster.hierarchy import cut_tree
#Let's check the silhouette score first to identify the ideal number of clusters
from sklearn.metrics import silhouette_score


# In[2]:


country_data = pd.read_csv('/home/aarush100616/Downloads/Projects/Humanitarian Aid/Data/Country-data.csv')
country_data.head()


# In[3]:


country_data.shape


# In[4]:


country_data.columns


# In[5]:


country_data.info()


# In[6]:


#Identifying Missing data
total_null = country_data.isnull().sum().sort_values(ascending = False)
percent = ((country_data.isnull().sum()/country_data.isnull().count())*100).sort_values(ascending = False)
print("Total records (country_data Data) = ", country_data.shape[0])
missing_data = pd.concat([total_null,percent.round(2)],axis=1,keys=['Total Missing','In Percent'])
missing_data.head(5)


# In[7]:


country_data.dtypes


# In[8]:


country_data.describe()


# In[9]:


# Converting exports,imports & health spending percentages to absolute values.
country_data['exports'] = country_data['exports'] * country_data['gdpp']/100
country_data['imports'] = country_data['imports'] * country_data['gdpp']/100
country_data['health'] = country_data['health'] * country_data['gdpp']/100
country_data.head(10)


# In[10]:


fig = plt.figure(figsize=(14,6))
fig.set_facecolor("lightgrey")

plt.subplot(2,3,1)
# Health :Total health spending as %age of Total GDP.
bottom10_health = country_data[['country','health']].sort_values('health', ascending = True).head(10)
sns.barplot(x='country',y='health',data=bottom10_health,palette="BuGn_r")
plt.title("Top 10 Countries with lowest spent on Health overall gdp",fontsize=9)
plt.xticks(rotation = 90,fontsize=10,family = "Comic Sans MS")

# Exports: Exports of goods and services. Given as %age of the Total GDP
plt.subplot(2,3,2)
bottom10_export = country_data[['country','exports']].sort_values('exports', ascending = True).head(10)
sns.barplot(x='country',y='exports',data=bottom10_export,palette="Blues")
plt.title("Top 10 Countries with lowest exports",fontsize=9)
plt.xticks(rotation = 90,fontsize=10,family = "Comic Sans MS")

# Imports: Imports of goods and services. Given as %age of the Total GDP
plt.subplot(2,3,3)
bottom10_import = country_data[['country','imports']].sort_values('imports', ascending = True).head(10)
sns.barplot(x='country',y='imports',data=bottom10_import,palette="Reds")
plt.title("Top 10 Countries with lowest imports",fontsize=9)
plt.xticks(rotation = 90,fontsize=10,family = "Comic Sans MS")


fig = plt.figure(figsize=(14,6))
fig.set_facecolor("lightgrey")

plt.subplot(2,3,1)
# Child Death Rate : Death of children under 5 years of age per 1000 live births
top10_deathrate = country_data[['country','child_mort']].sort_values('child_mort',ascending=False).head(10)
sns.barplot(x='country',y='child_mort',data=top10_deathrate,palette="BuGn_r")
plt.title("Top 10 Countries with highest child Death Rate",fontsize=9)
plt.xticks(rotation = 90,fontsize=10,family="Comic Sans MS")

plt.subplot(2,3,2)
# Fertility Rate: The number of children that would be born to each woman if the current age-fertility rates remain the same
top10_tot_fer = country_data[['country','total_fer']].sort_values('total_fer', ascending = False).head(10)
sns.barplot(x='country',y='total_fer',data=top10_tot_fer,palette="Blues")
plt.title("Top 10 Countries with highest Fertility Rate",fontsize=9)
plt.xticks(rotation = 90,fontsize=10,family = "Comic Sans MS")

# Life Expectancy: The average number of years a new born child would live if the current mortality patterns are to remain same
plt.subplot(2,3,3)
top10_lowest_life_expec = country_data[['country','life_expec']].sort_values('life_expec', ascending = True).head(10)
sns.barplot(x='country',y='life_expec',data=top10_lowest_life_expec,palette="Reds")
plt.title("Top 10 Countries with lowest life Expectancy",fontsize=9)
plt.xticks(rotation = 90,fontsize=10,family = "Comic Sans MS")

fig = plt.figure(figsize=(14,6))
fig.set_facecolor("lightgrey")

plt.subplot(2,3,1)
# The GDP per capita : Calculated as the Total GDP divided by the total population.
bottom10_gdpp = country_data[['country','gdpp']].sort_values('gdpp',ascending=True).head(10)
sns.barplot(x='country',y='gdpp',data=bottom10_gdpp,palette="BuGn_r")
plt.title("Bottom 10 Countries with overall gdpp",fontsize=9)
plt.xticks(rotation = 90,fontsize=10,family="Comic Sans MS")

plt.subplot(2,3,2)
# Per capita Income : Net income per person
bottom10_net_income = country_data[['country','income']].sort_values('income', ascending = True).head(10)
sns.barplot(x='country',y='income',data=bottom10_net_income,palette="Blues")
plt.title("Bottom 10 Countries with net income per person",fontsize=9)
plt.xticks(rotation = 90,fontsize=10,family = "Comic Sans MS")

# Inflation: The measurement of the annual growth rate of the Total GDP
plt.subplot(2,3,3)
bottom10_inflation = country_data[['country','inflation']].sort_values('inflation', ascending = False).head(10)
sns.barplot(x='country',y='inflation',data=bottom10_inflation,palette="Reds")
plt.title("Bottom 10 Countries with inflation rate",fontsize=9)
plt.xticks(rotation = 90,fontsize=10,family = "Comic Sans MS")
plt.show()


# In[11]:


# Correlation coefficients to see which variables are highly correlated
plt.figure(figsize = (16, 10))
sns.heatmap(country_data.corr(),annot=True,cmap="Greens")
plt.show()


# In[12]:


# Pairplot of all numeric columns
sns.pairplot(country_data)


# In[13]:


country_data_tmp = country_data.copy()
country_data_tmp.head()


# In[14]:


# Create a scaling object
scaler = StandardScaler()
# Create a list of the variables that you need to scale
col_list = ['child_mort', 'exports', 'health', 'imports', 'income', 'inflation', 'life_expec', 'total_fer', 'gdpp']
# Scale these variables using 'fit_transform'
country_data_tmp[col_list] = scaler.fit_transform(country_data_tmp[col_list])
country_data_tmp.head()


# In[15]:


# Putting feature variable to x
x = country_data_tmp.drop(['country'],axis=1)
# Putting response variable to country
country = country_data_tmp['country']


# In[16]:


x.shape
x.head()


# In[17]:


country.shape
country.head()


# ## Applying PCA to data

# In[18]:


pca = PCA(svd_solver='randomized',random_state=42)
pca.fit(x)


# In[19]:


pca.components_


# In[20]:


# Variance Ratio
pca.explained_variance_ratio_


# In[21]:


# Variance Ratio bar plot for each PCA components.
fig = plt.figure(figsize = (8,6))
plt.bar(range(1,len(pca.explained_variance_ratio_)+1), pca.explained_variance_ratio_)
plt.xlabel("PCA Components",fontsize=10,family = "Comic Sans MS")
plt.ylabel("Variance Ratio",fontsize=10,family = "Comic Sans MS")
plt.title("Variance Ratio for each PCA component",fontsize=14,family = "Comic Sans MS")


# In[22]:


fig = plt.figure(figsize = (8,6))
fig.set_facecolor("lightgrey")
var_cumu = np.cumsum(pca.explained_variance_ratio_)
plt.plot(range(1,len(var_cumu)+1), var_cumu)
plt.vlines(x=3,ymax=1,ymin=0.0,color="g",linestyles="--")
plt.hlines(y=0.88,xmax=8,xmin=0.0,color="b",linestyles="--")
plt.xlabel('Number of PCA Components',fontsize=12,family = "Comic Sans MS")
plt.ylabel('Cumulative Explained Variance',fontsize=12,family = "Comic Sans MS")
plt.title("Sree plot to Visualize Cumulative Variance",fontsize=14,family = "Comic Sans MS")


# In[23]:


# Checking which attributes are well explained by the pca components
colnames = list(x.columns)
pca_attr = pd.DataFrame({'Attribute':colnames,'PC1':pca.components_[0],'PC2':pca.components_[1],'PC3':pca.components_[2]})
pca_attr


# In[24]:


# Plotting the above dataframe for better visualization with PC1 and PC2
sns.set(style='darkgrid')
sns.pairplot(data=pca_attr, x_vars=["PC1"], y_vars=["PC2"], hue = "Attribute" ,height=8)
plt.xlabel("Principal Component 1",fontsize=12,family = "Comic Sans MS")
plt.ylabel("Principal Component 2",fontsize=12,family = "Comic Sans MS")

for i,txt in enumerate(pca_attr.Attribute):
    plt.annotate(txt, (pca_attr.PC1[i],pca_attr.PC2[i]))


# In[25]:


# Building the dataframe using Incremental PCA for better efficiency.
inc_pca = IncrementalPCA(n_components=3)


# In[26]:


pca_final = inc_pca.fit_transform(x)
pca_final.shape


# In[27]:


pca_final = pd.DataFrame(pca_final, columns=["PC1", "PC2","PC3"])
df = pd.concat([country, pca_final], axis=1)
df.head()


# In[28]:


# Plotting Heatmap to check is there still dependency in the dataset.

plt.figure(figsize = (8,6))        
ax = sns.heatmap(df.corr(),annot = True,cmap="Blues")


# In[29]:


sns.set(style='white')
fig.set_facecolor("lightgrey")

plt.figure(figsize=(20, 8))
plt.subplot(2,3,1)
sns.scatterplot(data=df, x='PC1', y='PC2')
plt.subplot(2,3,2)
sns.scatterplot(data=df, x='PC1', y='PC3')
plt.subplot(2,3,3)
sns.scatterplot(data=df, x='PC3', y='PC2')


# In[30]:


#Function to plot a list of categorical variables together
def box_plot(colname):
    plt.figure(figsize=(15, 4))
    for var in colname:
        plt.subplot(1,3,colname.index(var)+1)
        sns.boxplot(x = var, data = df)
        plt.xlabel(var, fontsize=12,family = "Comic Sans MS")
    plt.show()


# In[31]:


colnames = ['PC1', 'PC2', 'PC3']
box_plot(colnames[:])


# In[32]:


# Statstical Outlier treatment for PC1

Q1 = df.PC1.quantile(0.05)
Q3 = df.PC1.quantile(0.95)
IQR = Q3 - Q1
df = df[(df.PC1 >= Q1) & (df.PC1 <= Q3)]

# Statstical Outlier treatment for PC2

Q1 = df.PC2.quantile(0.05)
Q3 = df.PC2.quantile(0.95)
IQR = Q3 - Q1
df = df[(df.PC2 >= Q1) & (df.PC2 <= Q3)]

# Statstical Outlier treatment for PC3
Q1 = df.PC3.quantile(0.05)
Q3 = df.PC3.quantile(0.95)
IQR = Q3 - Q1
df = df[(df.PC3 >= Q1) & (df.PC3 <= Q3)]


# In[33]:


colnames = ['PC1', 'PC2', 'PC3']
box_plot(colnames[:])


# In[34]:


# Reindexing the df after outlier removal
df = df.reset_index(drop=True)
df_final = df.drop(['country'],axis=1)
df.head()
df_final.shape


# ### Hopkins Statistics Test:
# * A way of measuring the cluster tendency of a data set.
# * A value close to 1 tends to indicate the data is highly clustered, random data will tend to result in values around 0.5, and uniformly distributed data will tend to result in values close to 0

# In[35]:


def hopkins(X):
    d = X.shape[1]
    n = len(X)
    m = int(0.1 * n) 
    nbrs = NearestNeighbors(n_neighbors=1).fit(X.values)
 
    rand_X = sample(range(0, n, 1), m)
 
    ujd = []
    wjd = []
    for j in range(0, m):
        u_dist, _ = nbrs.kneighbors(uniform(np.amin(X,axis=0),np.amax(X,axis=0),d).reshape(1, -1), 2, return_distance=True)
        ujd.append(u_dist[0][1])
        w_dist, _ = nbrs.kneighbors(X.iloc[rand_X[j]].values.reshape(1, -1), 2, return_distance=True)
        wjd.append(w_dist[0][1])
 
    HS = sum(ujd) / (sum(ujd) + sum(wjd))
    if isnan(HS):
        print(ujd, wjd)
        HS = 0
 
    return HS


# In[36]:


#Let's check the Hopkins measure
hopkins(df_final)


# In[37]:


# Elbow curve method to find the ideal number of clusters.
ssd = []
for num_clusters in list(range(1,10)):
    kmeans = KMeans(n_clusters = num_clusters, max_iter=50,random_state= 100)
    kmeans.fit(df_final)
    ssd.append(kmeans.inertia_)

plt.plot(ssd)


# In[38]:


# Silhouette score analysis to find the ideal number of clusters for K-means clustering
range_n_clusters = [2, 3, 4, 5, 6, 7, 8]

for num_clusters in range_n_clusters:    
    # intialise kmeans
    kmeans = KMeans(n_clusters=num_clusters, max_iter=50,random_state= 100)
    kmeans.fit(df_final)
    cluster_labels = kmeans.labels_
    
    # silhouette score
    silhouette_avg = silhouette_score(df_final, cluster_labels)
    print("For n_clusters={0}, the silhouette score is {1}".format(num_clusters, silhouette_avg))


# In[39]:


#K-means with k=4 clusters
cluster5 = KMeans(n_clusters=5, max_iter=50, random_state= 100)
cluster5.fit(df_final)
# Cluster labels
cluster5.labels_


# In[40]:


# Assign the label
df['Cluster_Id'] = cluster5.labels_
df.head()


# In[41]:


# Number of countries in each cluster
df['Cluster_Id'].value_counts()


# In[42]:


# Scatter plot on Principal components to visualize the spread of the data

fig, axes = plt.subplots(1,3, figsize=(15,7))

sns.scatterplot(x='PC1',y='PC2',hue='Cluster_Id',legend='full',palette="Set1",data=df,ax=axes[0])
sns.scatterplot(x='PC1',y='PC3',hue='Cluster_Id',legend='full',palette="Set1",data=df,ax=axes[1])
sns.scatterplot(x='PC2',y='PC3',hue='Cluster_Id',legend='full',palette="Set1",data=df,ax=axes[2])


# In[43]:


# Merging the df with PCA with original df
df_merge = pd.merge(country_data,df,on='country')
df_merge_col = df_merge[['country','child_mort','exports','imports','health','income','inflation','life_expec','total_fer','gdpp','Cluster_Id']]
# Creating df with mean values
cluster_child = pd.DataFrame(df_merge_col.groupby(["Cluster_Id"]).child_mort.mean())
cluster_export = pd.DataFrame(df_merge_col.groupby(["Cluster_Id"]).exports.mean())
cluster_import = pd.DataFrame(df_merge_col.groupby(["Cluster_Id"]).imports.mean())
cluster_health = pd.DataFrame(df_merge_col.groupby(["Cluster_Id"]).health.mean())
cluster_income = pd.DataFrame(df_merge_col.groupby(["Cluster_Id"]).income.mean())
cluster_inflation = pd.DataFrame(df_merge_col.groupby(["Cluster_Id"]).inflation.mean())         
cluster_lifeexpec = pd.DataFrame(df_merge_col.groupby(["Cluster_Id"]).life_expec.mean())
cluster_totalfer = pd.DataFrame(df_merge_col.groupby(["Cluster_Id"]).total_fer.mean())
cluster_gdpp = pd.DataFrame(df_merge_col.groupby(["Cluster_Id"]).gdpp.mean())
df_concat = pd.concat([pd.Series([0,1,2,3,4]),cluster_child,cluster_export,cluster_import,cluster_health,cluster_income
                       ,cluster_inflation,cluster_lifeexpec,cluster_totalfer,cluster_gdpp], axis=1)
df_concat.columns = ["Cluster_Id", "Child_Mortality", "Exports", "Imports","Health_Spending","Income","Inflation","Life_Expectancy","Total_Fertility","GDPpcapita"]
df_concat.head()


# In[44]:


df_merge_col.head(5)


# In[45]:


figsize=(15,12)
sns.scatterplot(x='income',y='child_mort',hue='Cluster_Id',data = df_merge_col,legend='full',palette="Set1")


# In[46]:


figsize=(15,12)
sns.scatterplot(x='child_mort',y='gdpp',hue='Cluster_Id',data=df_merge_col,legend='full',palette="Set1")


# In[47]:


figsize=(15,12)
sns.scatterplot(x='gdpp',y='income',hue='Cluster_Id',data=df_merge_col,legend='full',palette="Set1")


# In[48]:


# Box plot on Original attributes to visualize the spread of the data
fig, axes = plt.subplots(2,2, figsize=(15,12))
sns.boxplot(x = 'Cluster_Id', y = 'child_mort', data = df_merge_col,ax=axes[0][0])
sns.boxplot(x = 'Cluster_Id', y = 'income', data = df_merge_col,ax=axes[0][1])
sns.boxplot(x = 'Cluster_Id', y = 'inflation', data=df_merge_col,ax=axes[1][0])
sns.boxplot(x = 'Cluster_Id', y = 'gdpp', data=df_merge_col,ax=axes[1][1])


# In[49]:


# Box plot to visualise the mean value of few original attributes.

fig, axes = plt.subplots(2,2, figsize=(15,12))

sns.boxplot(x = 'Cluster_Id', y = 'Child_Mortality', data = df_concat,ax=axes[0][0])
sns.boxplot(x = 'Cluster_Id', y = 'Income', data = df_concat,ax=axes[0][1])
sns.boxplot(x = 'Cluster_Id', y = 'Inflation', data=df_concat,ax=axes[1][0])
sns.boxplot(x = 'Cluster_Id', y = 'GDPpcapita', data=df_concat,ax=axes[1][1])


# In[50]:


# List of countries in Cluster 0
df_merge_col[df_merge_col['Cluster_Id']==0]


# In[51]:


# List of countries in Cluster 3
df_merge_col[df_merge_col['Cluster_Id']==3]


# In[52]:


df_final.head()


# In[53]:


# Single linkage
single_link = linkage(df_final, method='single',metric='euclidean')
dendrogram(single_link)
plt.show()


# In[54]:


# Complete Linkage
complete_link = linkage(df_final, method='complete',metric='euclidean')
dendrogram(complete_link)
plt.show()


# In[55]:


df_hc = df.copy()
df_hc = df_hc.drop('Cluster_Id',axis=1)
df_hc.head()


# In[56]:


# Let cut the tree at height of approx 3 to get 4 clusters &
# see if it get any better cluster formation.
clusterCut = pd.Series(cut_tree(complete_link, n_clusters = 4).reshape(-1,))
df_hc_cut = pd.concat([df_hc, clusterCut], axis=1)
df_hc_cut.columns = ['country', 'PC1', 'PC2','PC3','Cluster_Id']
df_hc_cut.head()


# In[57]:


# Scatter plot on Principal components to visualize the spread of the data
fig, axes = plt.subplots(1,2, figsize=(15,8))
sns.scatterplot(x='PC1',y='PC2',hue='Cluster_Id',legend='full',palette="Set1",data=df_hc_cut,ax=axes[0])
sns.scatterplot(x='PC1',y='PC3',hue='Cluster_Id',legend='full',palette="Set1",data=df_hc_cut,ax=axes[1])


# In[58]:


# Merging the df with PCA with original df

df_merge_hc=pd.merge(country_data,df_hc_cut,on='country')
df_merge_hc_col=df_merge_hc[['country','child_mort','exports','imports','health','income','inflation','life_expec','total_fer','gdpp','Cluster_Id']]
df_merge_hc_col.head()


# In[59]:


df_merge_hc_col['Cluster_Id'].value_counts()


# In[60]:


figsize=(15,12)
sns.scatterplot(x='income',y='child_mort',hue='Cluster_Id',data = df_merge_hc_col,legend='full',palette="Set1")


# In[61]:


figsize=(15,12)
sns.scatterplot(x='child_mort',y='gdpp',hue='Cluster_Id',data=df_merge_hc_col,legend='full',palette="Set1")


# In[62]:


figsize=(15,12)
sns.scatterplot(x='gdpp',y='income',hue='Cluster_Id',data=df_merge_hc_col,legend='full',palette="Set1")


# In[63]:


# Box plot on Original attributes to visualize the spread of the data
fig, axes = plt.subplots(2,2, figsize=(15,12))

sns.boxplot(x = 'Cluster_Id', y = 'child_mort', data = df_merge_hc_col,ax=axes[0][0])
sns.boxplot(x = 'Cluster_Id', y = 'income', data = df_merge_hc_col,ax=axes[0][1])
sns.boxplot(x = 'Cluster_Id', y = 'inflation', data=df_merge_hc_col,ax=axes[1][0])
sns.boxplot(x = 'Cluster_Id', y = 'gdpp', data=df_merge_hc_col,ax=axes[1][1])


# In[64]:


# List of countries in Cluster 0
df_merge_hc_col[df_merge_hc_col['Cluster_Id']==0]


# In[65]:


country_list = df_merge_hc_col[df_merge_hc_col['Cluster_Id']==0]
country_list.head()


# In[66]:


country_list['country']


# In[67]:


country_childmort = pd.DataFrame(country_list.groupby(['country'])['child_mort'].mean().sort_values(ascending = False))
country_childmort.plot.bar(figsize=(15,10),facecolor='g')
plt.title('Country vs Child Mortality',fontsize=16,family = "Comic Sans MS")
plt.xlabel("Country",fontweight = 'bold')
plt.ylabel("Child Mortality", fontsize = 12, fontweight = 'bold')
plt.show()


# In[68]:


# BarPlot for Per Capita Income of countries which are in need of aid

country_income = pd.DataFrame(country_list.groupby(['country'])['income'].mean().sort_values(ascending = True))
country_income.plot.bar(figsize=(15,10),facecolor='b')
plt.title('Country vs Per Capita Income',fontsize=16,family = "Comic Sans MS")
plt.xlabel("Country",fontweight = 'bold')
plt.ylabel("Per Capita Income", fontsize = 12, fontweight = 'bold')
plt.show()


# In[69]:


# BarPlot for Per Capita Income of countries which are in need of aid

country_gdp = pd.DataFrame(country_list.groupby(['country'])['gdpp'].mean().sort_values(ascending = True))
country_gdp.plot.bar(figsize=(15,10),facecolor='r')
plt.title('Country vs GDP per capita',fontsize=16,family = "Comic Sans MS")
plt.xlabel("Country",fontweight = 'bold')
plt.ylabel("GDP per capita", fontsize = 12, fontweight = 'bold')
plt.show()


# In[70]:


# Final countries list
country_list.reset_index(drop=True).country

